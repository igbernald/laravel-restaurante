<?php

use Illuminate\Support\Facades\Route;
use RealRashid\SweetAlert\Facades\Alert;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::resource('saucer', 'saucerController');
// Route::resource('ingredients', 'ingredientsController');
Route::post('saucer/{saucer}/ingredients','ingredientsController@store');
Route::get('saucer/{saucer}/ingredients','ingredientsController@index');
