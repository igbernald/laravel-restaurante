<?php

use Illuminate\Database\Seeder;
use Restaurant\Role;
use Restaurant\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
       $roleUser =  Role::where('name','user')->first();
       $roleAdmin = Role::where('name','admin')->first();

       $user = new User();
       $user->name="User";
       $user->email="user@gmail.com";
       $user->password=bcrypt('User');
       $user->save();
       $user->roles()->attach($roleUser); //Se hace la union de las dos tablas users con roles

       $user = new User();
       $user->name="Admin";
       $user->email="admin@gmail.com";
       $user->password=bcrypt('Admin');
       $user->save();
       $user->roles()->attach($roleAdmin);

    }
}
