<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaucerIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('saucerIngredients', function (Blueprint $table) {
            $table->bigIncrements('idSaucerIngredients');
            $table->string('ingredientName');
            $table->binary('image');
            $table->foreign('idSaucerIngredients')->references('id')->on('saucers');
            $table->string('slug')->unique();
            $table->integer('active')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('saucer_ingredients');
    }
}
