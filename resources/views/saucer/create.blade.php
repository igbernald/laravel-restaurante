@extends('layouts.layoutWithoutSideBar')
<link rel="icon" type="image/png" href="{{ asset('images/icons/platillos.svg') }}" rel="stylesheet">
@section('title', 'Platillo')
@section('content')
<body class="container-create-saucer">
    <div class="card-plantilla" style="margin-top:30px;">
        <h5 class="card-title" style="text-align: center; font-size:40px;padding-top:20px;">Platillos</h5>
        @if($errors->any())
        @foreach($errors->all() as $key => $error)
        <p>{{ $error}}</P>
        @endforeach
        @endif
        {!! Form::open(['route'=>'saucer.store','method'=>'POST', 'files'=>true ,'style' => 'padding: 50px;']) !!}
        @include('saucer.form')
        {!! Form::close()!!}
    </div>
</body>
@endsection
