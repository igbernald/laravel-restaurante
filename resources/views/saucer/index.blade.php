@extends('layouts.app')
<link rel="icon" type="image/png" href="{{ asset('images/icons/platillos.svg') }}" rel="stylesheet">
@section('title', 'Platillo')
@section('content')
<div class="">
    <div class="div-search-index">
        <form class="form-inline my-2 my-lg-0" style="float: right;">
            <a href="{{url('saucer/create')}}" class="btn btn-primary mr-sm-2 input-small"><i class="fa-fw fa-plus fa"></i></a>
            <input class="form-control-new mr-sm-2 input-small" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-success my-2 btn-sm" type="submit">Search</button>
        </form>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col" >Nombre</th>
                    <th scope="col">Descripción</th>
                    <th scope="col">Precio</th>
                    <th scope="col">Imagen</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach($saucers as $key=> $saucer)
                <tr class="table-info">
                    <td style="font-weight: 700;">{{$saucer->name}}</td>
                    <td>{{$saucer->descriptionSaucer}}</td>
                    <td>$ {{$saucer->price}}</td>
                    <td class="modal-container"><img src="{{ URL('/images/platillos/'.$saucer->image)}}" class="card-img-top card-img-new"></td>
                    {!! Form::open(['route' =>['saucer.destroy',$saucer->slug],'method'=>'DELETE']) !!}
                    {{-- <td> <a href="{{ URL('/saucer/'.$saucer->slug)}}" class="btn btn-primary btn-sm"><i class="fa-fw fa-edit fa"></i></a></td> --}}
                    <td> <a href="{{ URL('/saucer/'.$saucer->slug.'/edit' )}}" class="btn btn-warning btn-sm""><i class="fa-fw fa-edit fa"></i></a>
                        {{ Form::button('<i class="fa fa-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-sm'] )  }}</td>
                    {!! Form::close() !!}
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="pagination-new">
    {{$saucers -> links() }}
</div>
@endsection
