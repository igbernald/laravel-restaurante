@extends('layouts.app')
@section('title', 'Platillo Edit')
@section('content') <nav class="navbar navbar-expand-md navbar-light bg-transparent">
    <div>
        <ul class="nav-side navbar-nav side-nav">
            <li>
                <a href="{{ url('/') }}" data-toggle="collapse" data-target="#submenu-1"><i class="fa fa-fw fa-search"></i>Platillos<i class="fa fa-fw fa-angle-down pull-right"></i></a>
            </li>
            <li>
                <a href="{{ url('/') }}"><i class="fa fa-fw fa-user-plus"></i>Usuarios</a>
            </li>
            <li>
                <a href="{{ url('/') }}"><i class="fa fa-fw fa-paper-plane-o"></i>Bebidas</a>
            </li>
            <li>
                <a href="{{ url('/') }}"><i class="fa fa-fw fa fa-question-circle"></i>Postres</a>
            </li>
            <li>
                <a href="{{ url('/') }}" data-toggle="collapse" data-target="#submenu-2"><i class="fa fa-fw fa-star"></i> MENU 2 <i class="fa fa-fw fa-angle-down pull-right"></i></a>
                <ul id="submenu-2" class="collapse">
                    <li><a href="{{ url('/') }}"><i class="fa fa-angle-double-right"></i> SUBMENU 2.1</a></li>
                    <li><a href="{{ url('/') }}"><i class="fa fa-angle-double-right"></i> SUBMENU 2.2</a></li>
                    <li><a href="{{ url('/') }}"><i class="fa fa-angle-double-right"></i> SUBMENU 2.3</a></li>
                </ul>
            </li>
        </ul>
    </div>
    @endsection
