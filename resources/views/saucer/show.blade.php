@extends('layouts.layoutWithoutSideBar')
<link rel="icon" type="image/png" href="{{ asset('images/icons/platillos.svg') }}" />
@section('title', 'Platillo')
@section('content')
<modal-button></modal-button>
<create-ingredient></create-ingredient>
<img src="/images/{{$saucer->image}}" class="card-img-top rounded-circle mx-auto d-block" style="height: 130px; width:130px; background-color:#EFEFEF; margin:20px;">
<div class="text-center">
    <h5 class="card-title">{{$saucer->name}}</h5>
    <p>{{$saucer->descriptionSaucer}}</p>
</div>
<list-ingredients></list-ingredients>
<div class="card mb-3" style="max-width: 540px;">
    <div class="row no-gutters">
        <div class="col-md-4">
            <img src="{{ asset('images/1584152918MoleRojo.jpg') }}" class="card-img-top mx-auto d-block" style="height: 130px; width:130px; margin-top:15px; margin-bottom:15px; margin-left:15px;">
        </div>
        <div class="col-md-8">
        
            <div class="card-body">
                <h5 class="card-title">Ingrediente 1</h5>
                <p class="card-text">Aqui va la descripcion del ingrediente</p>
                <a href="/saucer/{{$saucer->slug}}/edit" class="btn btn-warning btn-sm">Editar</a>
            </div>
        </div>
    </div>
</div>
<div class="text-center">
    <a href="/saucer/{{$saucer->slug}}/edit" class="btn btn-primary">Nuevo Ingrediente</a>
    <a href="{{ url('/saucer') }}" class="btn btn-danger">Cancelar</a>
</div>
@endsection
