@extends('layouts.layoutWithoutSideBar')
<link rel="icon" type="image/png" href="{{ asset('images/icons/platillos.svg') }}" rel="stylesheet">
@section('title', 'Platillo Edit')
@section('content')
<body class="container-edit-saucer" style="margin-top:30px;">
    <div class="card-saucer-edit">
        <h5 class="card-title" style="text-align: center; font-size:40px;padding-top:20px;">Editar Platillo</h5>
        {!! Form::model($saucer, ['route' =>['saucer.update',$saucer],'method'=>'PUT', 'files'=>true,'style' => 'padding: 50px;']) !!}
        <img src="/images/platillos/{{$saucer->image}}" class="card-img-top mx-auto d-block img-edit-saucer">
        @include('saucer.form')
        {!! Form::close()!!}
    </div>
</body>
@endsection
