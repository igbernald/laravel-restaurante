<div class="form-row" style="margin-top:25px; padd">
    <div class="form-group col-md-6">
        {!! Form::label('name','Platillo',['class' => '']) !!}
        {!! Form::text('name',null,['class' => 'form-control','placeholder'=>'Nombre de Platillo','required']) !!}
    </div>
    <div class="form-group col-md-6">
        {!! Form::label('name','Precio $',['class' => '']) !!}
        {!! Form::text('price',null,['class' => 'form-control','placeholder'=>'Precio','required']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('name','Descripción',['class' => '']) !!}
    {!! Form::textarea('descriptionSaucer',null,['class' => 'form-control','placeholder'=>'Descripción','required','rows' => 3, 'cols' => 40]) !!}
</div>
<div class="form-group">
    {!! Form::label('name','Imagen',['class' => '']) !!}
    <input type="file" class="form-control-file" name="image">
    {{-- {!! Form::file('image',null,['class' => 'form-control-file','required']) !!} --}}
</div>
<div class="input-group">
    {!! Form::submit('Guardar',['class' => 'form-control col-md-1 btn btn-outline-success','style' =>"margin-top: 34px;"]) !!}
    <a href="{{ url('/saucer') }}" class="btn btn-outline-danger form-control col-md-1" style="margin-left: 50px; margin-top: 34px;"">Cancelar</a>
</div>
