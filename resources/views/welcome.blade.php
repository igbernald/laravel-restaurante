<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="../css/app.css">
    <!-- Styles -->
    <link href="{{ asset('css/newStyles.css') }}" rel="stylesheet">
    <!-- Styles -->
    <link rel="icon" type="image/png" href="{{ asset('images/icons/restaurant.svg') }}" rel="stylesheet">
</head>
<body class="container-welcome">
    <div class="position-ref full-height">
        @if (Route::has('login'))
        <div class="top-right links" style="float: right; margin-right: 15px; font-size: 21px;">
            @auth
            <a href="{{ url('/home') }}">Inicio</a>
            @else
            <a href="{{ route('login') }}" style="color: black; padding-right: 20px;">Login</a>
            @if (Route::has('register'))
            <a href="{{ route('register') }}" style="color: black; padding-right: 20px;">Registro</a>
            @endif
            @endauth
        </div>
        @endif
        <div class="content">
            <div class="title m-b-md">
                <p>Restaurante</p>
            </div>
        </div>
    </div>
</body>
</html>
