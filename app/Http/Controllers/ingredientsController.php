<?php

namespace Restaurant\Http\Controllers;

use Illuminate\Http\Request;
use Restaurant\Ingredient;
use Restaurant\Saucer;

class ingredientsController extends Controller {

    public function index(Saucer $saucer, Request $request){
        if($request->ajax()){
            // $ingredients = Ingredient::all();
            return response()->json($saucer->ingredients, 200);
        }
        return view('ingredients.index');
    }

    public function store(Saucer $saucer, Request $request){
        if($request->ajax()){
            $ingredient = new Ingredient();
            $ingredient->name=$request->input('name');
            $ingredient->image=$request->input('image');
            $ingredient->saucer()->associate($saucer)->save();
            $ingredient->save();
            return response()->json([
                "saucer"=>$saucer,
                "message"=>"Ingrediente Creado correctamente",
                "ingredient"=>$ingredient
            ],200);
        }
    }
}
