<?php

namespace Restaurant\Http\Controllers;

use Restaurant\Saucer;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Restaurant\Http\Requests\storeSaucerRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class saucerController extends Controller{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){        
        
        // $request->user()->authorizeRoles('admin');//Autorización de los roles
        // $saucers = Saucer::where('active','1')->get();
        // $saucers = User::where('votes', '>', 100)->paginate(15);
        // $saucers = DB::table('saucers')->paginate(2);
        $saucers = Saucer::where('active','1')->paginate(5);
        return view('saucer.index',compact('saucers'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('saucer.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $saucer = new Saucer();
        if($request->hasFile('image')){
            $file = $request->file('image');
            $nameFile = time().$file->getClientOriginalName();
            $file->move(public_path().'/images/platillos',$nameFile);
        }
        $saucer->name = $request->input('name');
        $saucer->price = $request->input('price');
        $saucer->descriptionSaucer= $request->input('descriptionSaucer');
        $saucer->image = $nameFile;
        $saucer->slug = $request->input('name');
        $saucer->save(); 
        return redirect('saucer')->with('success', 'El Registro ha sido guardado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Saucer $saucer){       
        // $saucer = Saucer::find($id);
        // $saucer = Saucer::where('slug','=',$slug)->firstOrFail();
        return view('saucer.show',compact('saucer'));
        // return $slug;
    }

    /**
     * Show the form for editing the specified resource.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Saucer $saucer){
        return view('saucer.edit',compact('saucer'));
    }

    /**
     * Update the specified resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Saucer $saucer){
        $saucer->fill($request->except('image'));
        // $saucer->fill($request->except('slug'));
        $saucer->slug = $request->input('name');
        if($request->hasFile('image')){
            $file = $request->file('image');
            $nameFile = time().$file->getClientOriginalName();
            $saucer->image=$nameFile;
            $file->move(public_path().'/images/platillos',$nameFile);
        }
        $saucer->save();
        return redirect('saucer')->with('success', 'El Registro ha sido guardado');
    }
    /**
     * Remove the specified resource from storage.
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Saucer $saucer){
        $filePath= public_path().'/images/'.$saucer->image;
        \File::delete($filePath);
        $saucer->active = 0;
        $saucer->save();
        return redirect('saucer');
    }
}