<?php

namespace Restaurant;

use Illuminate\Database\Eloquent\Model;

class Saucer extends Model{
    protected $fillable=['name','price','descriptionSaucer','image','active'];
    /**
     * Get the route key for the model.
     * @return string
     */
    public function getRouteKeyName(){
        return 'slug';
    }
    public function ingredients(){
        return $this->hasMany('Restaurant\ingredient');
    }
}