<?php

namespace Restaurant;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model {

    public function saucer(){
        return $this->belongsTo('Restaurant\Saucer');
    }
}
