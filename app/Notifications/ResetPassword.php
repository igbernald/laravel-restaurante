<?php

namespace Restaurant\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResetPassword extends Notification
{
    use Queueable;
    protected $token;
    protected $url;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
        $this->url = $url = url(config('app.url').route('password.reset', [
            'token' => $this->token,
        ], false));
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->salutation('¡Saludos!')
            ->subject('Solicitud para restablecer contraseña')
            ->line('Hemos recibido esta petición')
            ->greeting('Hola '. $notifiable->name)
            ->line('Restablecimiento de tu contraseña.')
            ->action('Actualizar contraseña', url($this->url))
            ->line('Si usted no ha realizado esta acción, favor de ignorar este correo!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
